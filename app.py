from flask import Flask, request, render_template

app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = request.form['dividend']
    divisor = request.form['divisor']

    return ''

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    grade = request.form['grade']

    return ''

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    value = request.form['value']
    return ''

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    number = request.form['number']
    return ''

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    number = request.form['number']
    return ''